#!/bin/sh
#
# Makes your bspwm focused window border more interesting.

cleanup() {
	bspc config focused_border_color "#817f7f"
}

trap cleanup EXIT

while :; do

	# ROYGBIV
	for k in \
		"#ff0000" \
		"#ff7f00" \
		"#ffff00" \
		"#00ff00" \
		"#0000ff" \
		"#4b00b2" \
		"#8f00ff" \
	; do
		sleep 0.1
		bspc config focused_border_color "$k"
	done
done
