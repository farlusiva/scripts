#!/bin/bash
# Prøver å øke batteritiden

level1() {

	# TODO: Drep pulseaudio
	echo "Level 1 power-saving activated."

	echo "Forcing CPU to 800MHz"
	# Sannsynligvis overkill.
	command -v cpupower >/dev/null && sudo cpupower frequency-set -u 800MHz

	echo "Turning bluetooth off..."
	command -v bluetooth >/dev/null && sudo bluetooth off

	killall lemonbar 2>/dev/null

}

level2() {
	# For bruk i TTY, ikke GUI

	if pgrep Xorg; then
		echo "You aren't running this from a tty. Exiting".
		exit 0
	fi

	# bare 'level3' vil løpe level3, men 'level3 down' vil
	# også løpe level2, som også løper level1 osv.
	if [[ $1 == "down" ]]; then
		level1 down
	fi


	echo "Level 2 power-saving activated."

	echo "Turning wifi off..."
	# ikke sudo wifi off, siden det bruker tlp og er det etter en reobot
	sudo rmmod iwldvm
	sudo rmmod iwlwifi
	sudo systemctl stop wpa_supplicant
	sudo systemctl stop NetworkManager

	# Dreper X umidderlbart, for en eller annen grunn
	echo "Stopping the dbus dameon..."
	sudo systemctl stop dbus.service


	if bc <<< "$(light) > 10" > /dev/null; then
		echo "Reducing screen brightness"
		light -S 10
	fi

}



case $1 in

	"-l"|"--level")

		case $2 in

		"1")
			level1 down
			;;

		"2")
			level2 down
			;;

		esac
		;;

	"-h")

		echo "$0: Kills proceses to save power"
		echo "Arguments: "
		echo "-l, --level [1-3]  Go more minimalist"
		echo "-h                 Display this message"
		exit 0

	;;


esac

killall dropbox \
redshift geoclue \
compton \
unclutter \
2>/dev/null

exit 0
