#!/bin/sh
#
# rec.sh
#
# Record you screen. Currently doesn't take audio (I think).

for k in screenkey ffmpeg; do
	if ! command -v "$k" >/dev/null 2>&1; then
		echo "$k not installed; exiting"
		exit
	fi
done

if [ "$1" = "start" ]; then
	screenkey &
	ffmpeg -video_size 1920x1080 -framerate 30 -f x11grab -i :0 ~/output.mp4 \
	       >/dev/null 2>&1
fi

if [ "$1" = "stop" ]; then
	pkill screenkey
	pkill ffmpeg
fi
