#!/bin/sh

DIR="/sys/devices/platform/smapi/BAT0"

# Sane enviorment
command -v awk >/dev/null && [ -d "$DIR" ] || exit 1

# Power usage, Watthours left
POWER="$(awk '{print $1 / -1000; exit}' "$DIR/power_avg")"
BAT="$(awk '{print $1 / 1000; exit}' "$DIR/remaining_capacity")"

printf "Reported power usage: %.2f W\n" "$POWER"
printf "Power left in battery: %.2f Wh\n" "$BAT"

# Time left
TIME="$(echo "$BAT $POWER" | awk '{print $1 / $2; exit}')"

printf "This should net you %.2f h\n" "$TIME"
