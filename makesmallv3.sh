#!/usr/bin/env bash
#
# makesmallv3.sh
# Version 3 of makesmall.sh.
# The two previous ones were lost
#
# Because of wierd ffmpeg things with multithreading, it's suggested to output
#  the ffmpeg commands to a separate file, and then run that file with bash.
#
# With the threads thing, essentially one in every $THREADS ffmpeg-job is done
#  without being parallelised, with helps ensure that the script won't start
#  like 1000 jobs at once, which could OOM-kill some of the jobs.
#
# NOTE: Be *very* careful with files that have multiple spaces in their name
#        as a (now fixed) bug with echo made those files not transcode.
#
# TODO:
#     - Make *all* directories be preserved, not the last one
#        Maybe use mkdir -p?


# The following, up to BASHFILE can be changed
THREADS=4 # How many threads makesmall should use

FOLDERS="$HOME/Archive/Music /mnt/hdd/Music /mnt/hdd/OtherMusic"
#FOLDERS="$HOME/Archive/Music"
#FOLDERS=""

OUT="$HOME/Archive/OUT"
BASHFILE="$HOME/makesmall_cmds.sh"
#BASHFILE=/dev/stdout



if [[ -z $HOME ]]; then
	echo "Can't find \$HOME. Change the instances of \$HOME in the code with "\
	     "something else or remove the code that prints this message."
	exit 1
fi

if [[ -d $OUT || -f $BASHFILE ]]; then
	echo "$OUT or $BASHFILE exists. Aborting."
	exit 1
fi

echo -n "" > "$BASHFILE"
mkdir "$OUT"

num=1

find $FOLDERS -regex ".*flac\|.*mp[34]\|.*mkv\|.*webm\|.*m4a" | while read k; do

	# Make folder if it doesn't exist
	[[ ! -d $OUT/$DIR ]] && mkdir "$OUT/$DIR"

	# Perl regexes!
	DIR="$(realpath "$k" | perl -pe "s|.*/(.*?)/(.*)\..{1,4}|\1|")"
	NAME="$(realpath "$k" | perl -pe "s|.*/(.*?)/(.*)\..{1,4}|\2|")"
	NEWFNAME="$OUT/$DIR/$NAME"

	case $k in
		*.flac)
			printf 'opusenc "%s" --bitrate 96k --vbr "%s"%s\n' "$k" \
			"$NEWFNAME.opus" "$( (( num != 0 )) && printf ' &')" >> "$BASHFILE"
		;;

		*.mp4 | *.mkv | *.webm | *.m4a)
			printf 'ffmpeg -i "%s" -b:a 96k "%s"%s < /dev/null\n' "$k" \
			"$NEWFNAME.mp3" "$( ((num != 0)) && printf ' &')" >> "$BASHFILE"
		;;

		*.mp3 | *.aac | *.opus)
			printf 'cp "%s" "%s"%s\n' "$k" \
			"$NEWFNAME.mp3" "$( ((num != 0)) && printf ' &')" >> "$BASHFILE"
		;;

	esac


	num=$(( (num+1) % THREADS ))

done
