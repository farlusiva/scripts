#!/bin/sh
# /r/unixporn/comments/8gwcti/motd_ubuntu_server_1804_lts_my_motd_scripts_for
# /r/unixporn/comments/9g94md/motd_motd_on_my_ubuntu_home_server


for k in figlet awk df uname ps uptime; do
	command -v "$k" >/dev/null || { echo "$k not installed, aborting."; exit; }
done

# red
printf '\033[1m\033[31m%s\033[0m' "$(figlet -f slant "$HOSTNAME")"

#printf '\n  %s\n\n' "$(uptime -p)"


printf '\nsystem info:\n'
printf '    Distro......: %s\n' "$(awk -F'"' '/^PRETTY_NAME=/ {print $2; exit}' /etc/*release)"
printf '    Kernel......: %s %s\n\n' "$(uname -s)" "$(uname -r)"
printf '    Uptime......: %s\n' "$(uptime -p)"
#printf '    Load........: %s\n' "$(sed -n -e "s/.\/.*//" -e "s/ /  /g" -e "s/  / (1m) /" -e "s/  / (5m) /" -e "s/  / (15m) /" -e "/^/p" /proc/loadavg)"
printf '    Load........: %s\n' "$(awk '{ print $1" (1m) " $2 " (5m) "$3" (15m)"}' /proc/loadavg)"
printf '    Processes...: %s\n\n' "$(ps -eo user | awk '/root/ {ROOT+=1} /'$USER'/ {USER+=1} {SUM+=1} END {printf ROOT" (root) "USER" (user) | "SUM" (sum)"}')"
printf '    CPU.........: %s\n' "$(awk '/^model name/ {gsub("^.*: ", ""); print; exit}' /proc/cpuinfo)"
printf '    Memory......: %s\n' "$(free --si -ht | awk '/Mem/ {printf $3" used, "$4" free, "$6" in total" }')"
printf '\n\n'



df -BG --output=source,size,used,avail,pcent,target | awk '

	(FNR == 1) {
		MAX=length($0) - 4 # Stops same place as the "Mounted on"

		gsub("1G-blocks", "    Space")
		gsub("Use%", "  % ")
		print
	}

	(FNR > 1 && /^\/dev\//) {

		PRCNT=$5
		gsub("%", "", PRCNT)
		LENGTH=int(MAX * PRCNT/100)

		printf("%s\n  [\033[34m", $0) # $0 contains a "%", treat it properly
		for (k = 0; k < LENGTH; k++) { printf("="); } # LENGTH equal signs
		printf("\033[90m")
		for (k = 0; k < MAX-LENGTH; k++) { printf("="); }
		printf("\033[0m]\n")
	}'


