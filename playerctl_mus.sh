#!/bin/sh
#
# playerctl_mus.sh
#
# This was written to be used in my dotfiles (instead of having a bind for just
# cmus, make it work for more than that), but I decided on not using it. It's
# kept here because I didn't want to delete it.
#
# Example usage (in sxhkd):
# alt + {Down,Up,Left,Right}
# 	playerctl_mus.sh {play-pause,stop,previous,next}
#
#
# On arch, requires mpv-mpris (aur) to be installed to work on mpv.
# On NixOS, install mpv with the mpris script (see mpv.nix)
#
# According to hyperfine, this script takes about 25 ms to do a play-pause.
# This should be optimized.
# In comparison, cmus-remote -Q hovers around 2 ms.

_awk() {
	busybox awk "$@"
}

# Prioritise the mpris instances as follows:
# 1) mpv
# 2) firefox
# 3) cmus
# 5) others

# (This code assumes that none of the playernames have \n's in them)

sort_instances() {
	 _awk '{

		if       (/^mpv$/) {
			print("1 " $0)
		} else if (/^firefox$/) { #
			print("2 " $0)
		} else if (/^cmus/) {
			print("3 " $0)
		} else {
			print("4 " $0)
		}

	}' | sort -h | cut -c3-
}

# Example data:
#printf "mpv\ncmus\nfirefox\nrandom_music_player" | sort_instances
# Expected output; mpv, firefox, cmus, random_music_player


# Get the currently playing instances

PLAYING="$(
playerctl -a metadata -f "{{playerInstance}} {{status}}" | grep "Playing" | sed "s/ Playing$//"
)"

# Prioritise currently playing instances before the others
INSTANCES="$(
	[ -n "$PLAYING" ] && echo "$PLAYING" | sort_instances
	playerctl -l | sort_instances
)"

# The instance to use
INST="$(echo "$INSTANCES" | head -n 1)"

playerctl -p "$INST" "$1"
